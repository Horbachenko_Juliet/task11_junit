package com.epam.minesweeper;

import com.epam.minesweeper.view.MyView;

public class Main {
    public static void main(String[] args) {
        new MyView().show();
    }
}
