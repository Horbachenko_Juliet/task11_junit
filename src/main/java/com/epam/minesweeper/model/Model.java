package com.epam.minesweeper.model;

public interface Model {
    void createMinefield(int minefieldLength, int minefieldHeight, double mineProbability);

    void replaceWithNumbers();
}
