package com.epam.minesweeper.model;

import java.util.Random;

public class Cell {
    private static boolean mineNumbersVisible;
    int row;
    int column;
    private boolean isMine;
    private int numbersOfNearbyMines = 0;
    private Random random = new Random();

    public Cell(double mineProbability, int row, int column) {
        double rnd = random.nextDouble();
        isMine = rnd < mineProbability;
        this.row = row;
        this.column = column;
    }

    int countMiles(int minefieldLength, int minefieldHeight, Cell[][] cells) {
        mineNumbersVisible = true;
        for (Cell[] cell : cells) {
            for (Cell c : cell) {
                for (int i = c.column - 1; i <= c.column + 1; ++i) {
                    for (int j = c.row - 1; j <= c.row + 1; ++j) {
                        if (i >= 0 && i < minefieldLength && j >= 0 && j < minefieldHeight) {
                            if (cells[i][j].isMine) {
                                c.numbersOfNearbyMines++;
                                numbersOfNearbyMines = 0;
                            }
                        }
                    }
                }
            }
        }
        return numbersOfNearbyMines;
    }

    @Override
    public String toString() {
        if (mineNumbersVisible) {
            if (!isMine) {
                return String.valueOf(numbersOfNearbyMines);
            } else return "*";
        } else if (!isMine) {
            return ".";
        } else return "*";
    }
}
