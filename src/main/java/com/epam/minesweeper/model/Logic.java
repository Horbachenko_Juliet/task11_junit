package com.epam.minesweeper.model;

public class Logic implements Model {
    Minefield minefield;

    @Override
    public void createMinefield(int minefieldLength, int minefieldHeight, double mineProbability) {
        minefield = new Minefield(minefieldLength, minefieldHeight, mineProbability);
    }

    @Override
    public void replaceWithNumbers() {
        minefield. replaceWithNumbers();
    }

}
