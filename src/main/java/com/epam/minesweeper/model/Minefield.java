package com.epam.minesweeper.model;

public class Minefield {

    private int minefieldHeight;
    private int minefieldLength;
    private double minesProbability;
    Cell[][] cells;
    Cell cell = new Cell(0.3, -1,-1);

    public Minefield(int minefieldLength, int minefieldHeight, double minesProbability) {
        this.minesProbability = minesProbability;
        this.minefieldHeight = minefieldHeight;
        this.minefieldLength = minefieldLength;
        fillField(minefieldLength, minefieldHeight);
    }
   private void fillField(int minefieldHeight, int minefieldLength) {
        cells = new Cell[minefieldHeight][minefieldLength];
        for (int i = 0; i < minefieldHeight; i++) {
            for (int j = 0; j < minefieldLength ; j++) {
                cells[i][j] = new Cell(minesProbability, j, i);
            }
        }
        for (Cell[] arrCell : cells) {
            for ( Cell anArrCell :arrCell) {
                System.out.print(anArrCell + " ");
            }
            System.out.println();
        }
    }

    void replaceWithNumbers(){
       cell.countMiles(minefieldLength, minefieldHeight, cells);
        for (Cell[] arrCell : cells) {
            for ( Cell anArrCell :arrCell) {
                System.out.print(anArrCell + " ");
            }
            System.out.println();
        }
    }
}
