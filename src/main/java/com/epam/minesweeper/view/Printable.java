package com.epam.minesweeper.view;

public interface Printable {
    void print();
}
