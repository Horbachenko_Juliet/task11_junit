package com.epam.longest_plateau;


public class RealPlateau implements Plateau {
    private int startIndex;
    private int value;
    private int consecutiveValueCount = 0;

    public RealPlateau(int startIndex, int value) {
        this.startIndex = startIndex;
        this.value = value;
    }

    @Override
    public Plateau appendNextValue(int nextValue) {
        if (this.value == nextValue) {
            consecutiveValueCount++;
            return this;
        }
        if (this.value > nextValue) {
            return new NonPlateau(startIndex + (++consecutiveValueCount), value);
        }
        startIndex += ++consecutiveValueCount;
        this.value = nextValue;
        consecutiveValueCount = 0;
        return this;
    }

    @Override
    public int compareTo(Plateau plateau) {
        return plateau.compareCountTo(consecutiveValueCount);
    }

    @Override
    public int compareCountTo(int otherConsecutiveValueCount) {
        return otherConsecutiveValueCount - consecutiveValueCount;
    }

    @Override
    public String toString() {
        return "RealPlateau{" +
                "startIndex=" + startIndex +
                ", value=" + value +
                ", consecutiveValueCount=" + consecutiveValueCount +
                '}';
    }
}
