package com.epam.longest_plateau;

public class NonPlateau implements Plateau {
    private int value;
    private int startIndex;

    public NonPlateau(int startIndex, int value) {
        this.startIndex = startIndex;
        this.value = value;
    }

    @Override
    public Plateau appendNextValue(int nextValue) {
        startIndex++;
        if (this.value < nextValue) return new RealPlateau(startIndex, nextValue);
        this.value = nextValue;
        return this;
    }

    @Override
    public int compareCountTo(int otherConsecutiveValueCount) {
        return otherConsecutiveValueCount + 1;
    }

    @Override
    public int compareTo(Plateau plateau) {
        return plateau.compareCountTo(-1);
    }

    @Override
    public String toString() {
        return "NonPlataea";
    }
}
