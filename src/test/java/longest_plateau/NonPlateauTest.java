package longest_plateau;

import com.epam.longest_plateau.NonPlateau;
import com.epam.longest_plateau.Plateau;
import com.epam.longest_plateau.RealPlateau;
import org.junit.Test;

import static org.junit.Assert.*;

public class NonPlateauTest {

    @Test
    public void testAppendNextValue() {
        int i1 = 1;
        int i2 = 2;
        int i3 = 3;
        Plateau expectedPlateau = new NonPlateau(i1 + 2, i2);
        Plateau actualPlateau = new RealPlateau(i1, i3);
        actualPlateau = actualPlateau.appendNextValue(i2);
        assertEquals(expectedPlateau, actualPlateau);
    }
}

