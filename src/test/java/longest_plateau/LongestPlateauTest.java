package longest_plateau;

import com.epam.longest_plateau.LongestPlateau;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LongestPlateauTest {

    @Test
    public void testMain() {
        LongestPlateau actualPlateau = new LongestPlateau();
        String actual = String.format("longest Plateau is: %s", actualPlateau.findLongestPlateauIn(new int[]{1, 2, 3, 3, 3, 1}));
        String expected = "longest Plateau is: RealPlateau{startIndex=2, value=3, consecutiveValueCount=3}";
        assertEquals(expected, actual);
    }
}
