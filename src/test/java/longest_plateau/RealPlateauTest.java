package longest_plateau;

import com.epam.longest_plateau.Plateau;
import com.epam.longest_plateau.RealPlateau;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RealPlateauTest {

    @Test
    public void testAppendNextValue() {
        int i1 = 1;
        int i2 = 2;
        Plateau actualPlateau = new RealPlateau(i1, i2);
        actualPlateau.appendNextValue(i2);
        Plateau expectedPlateau = new RealPlateau(i1, i2);
        expectedPlateau.appendNextValue(i2);
        assertEquals(expectedPlateau, actualPlateau);
    }
}

